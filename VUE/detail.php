<?php
    // Récupérer l'ID du produit
    if(isset($_GET['ID']) && is_numeric($_GET['ID'])) {
        $productId = $_GET['ID'];
        
        // Charger les détails du produit
        require('../MODEL/data.php');
        $stmt = $pdo->prepare('SELECT * FROM produit WHERE ID = :ID');
        $stmt->bindParam(':ID', $productId, PDO::PARAM_INT);
        $stmt->execute();
        
        $productDetails = $stmt->fetch(PDO::FETCH_ASSOC);
    } else {
        echo "ID de produit invalide.";
        exit; // Arrêter le script si ça foire
    }
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="style.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <title>Product Details</title>
</head>
<body>
<?php include "header.php"; ?>

<div class="container">
    <div class="card">
        <div class="row">
            <aside class="col-sm-5 border-right">
                <article class="gallery-wrap"> 
                    <div class="img-big-wrap">
                        <div> <a href="#"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSwVTRKFFlmcwmJ3GQNA0-rptmy3OSIY2LnfQ&usqp=CAU"></a></div>
                    </div> <!-- slider-product.// -->
                    <div class="img-small-wrap">
                        <div class="item-gallery"> <img src="https://s9.postimg.org/tupxkvfj3/image.jpg"> </div>
                        <div class="item-gallery"> <img src="https://s9.postimg.org/tupxkvfj3/image.jpg"> </div>
                        <div class="item-gallery"> <img src="https://s9.postimg.org/tupxkvfj3/image.jpg"> </div>
                        <div class="item-gallery"> <img src="https://s9.postimg.org/tupxkvfj3/image.jpg"> </div>
                    </div> <!-- slider-nav.// -->
                </article> <!-- gallery-wrap .end// -->
            </aside>
            <!-- les modifs commencent ici -->
            <aside class="col-sm-7">
                <article class="card-body p-5">
                    <h3 class="title mb-3"><?= $productDetails['Nom'] ?></h3>

                    <p class="price-detail-wrap">
                        <span class="price h3 text-warning">
                            <span class="num"><?= $productDetails['Prix'] ?></span><span class="currency">euros</span>
                        </span>
                    </p>

                    <dl class="item-property">
                        <dt>Série</dt>
                        <dd><p><?= $productDetails['Serie'] ?></p></dd>
                    </dl>
                    <dl class="item-property">
                        <dt>Quantité en stock: </dt>
                        <dd><p><?= $productDetails['Quantite'] ?></p></dd>
                    </dl>
            <!-- et s'arrêtent là -->
                    <hr>
                    <div class="row">
                        <div class="col-sm-5">
                            <dl class="param param-inline">
                                <dt>Quantité: </dt>
                                <dd><select name="Quantite" id=""><?= $productDetails['Quantite'] ?></select></dd>
                            </dl>  <!-- item-property .// -->
                        </div> <!-- col.// -->
                    </div> <!-- row.// -->
                    <hr>
                    <a href="#" class="btn btn-lg btn-primary text-uppercase"> Ajouter au panier </a>
                </article> <!-- card-body.// -->
            </aside> <!-- col.// -->
        </div> <!-- row.// -->
    </div> <!-- card.// -->
</div>

<?php include "footer.php"; ?>

</body>
</html>
