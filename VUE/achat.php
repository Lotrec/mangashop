<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="achat.css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-4bw+/aepP/YC94hEpVNVgiZdgIC5+VKNBQNGCHeKRQN+PtmoHDEXuppvnDJzQIu9" crossorigin="anonymous">
    <title>Document</title>
</head>
<body>
<?php include "header.php"; ?>
    <div class="Container">
        <div class="BoiteGauche">
            <div class="wrapper">
                <h4 class="text-uppercase">Payment Details</h4>
                <form class="form mt-4">
                    <div class="form-group">
                        <label for="name" class="text-uppercase">name on the card</label>
                        <input type="text" class="form-control" placeholder="Nicolos Flemming">
                    </div>
                    <div class="form-group">
                        <label for="card" class="text-uppercase">card number</label>
                        <div class="card-number">
                            <input type="text" class="card-no" step="4" placeholder="1234 4567 5869 1234" pattern="^[0-9].{15,}">
                            <span class="">
                                <img src="https://www.freepnglogos.com/uploads/mastercard-png/mastercard-marcus-samuelsson-group-2.png"
                                alt="" width="30" height="30">
                            </span>
                        </div>
                    </div>
                    <div class="d-flex w-100">
                        <div class="d-flex w-50 pr-sm-4">
                            <div class="form-group">
                                <label for="expiry" class="text-uppercase">exp.date</label>
                                <input type="text" class="form-control" placeholder="03/2020">
                            </div>
                        </div>
                        <div class="d-flex w-50 pl-sm-5 pl-3">
                            <div class="form-group">
                                <label for="cvv">CVV</label>
                                <a href="#"
                                    title="CVV is a credit or debit card number, the last three digit number printed on the signature panel">what's
                                    this</a>
                                    <input type="password" class="form-control pr-5" maxlength="3" placeholder="123">
                            </div>
                        </div>
                    </div>
                    <div class="form-inline pt-sm-3 pt-2">
                        <input type="checkbox" name="address" id="address">
                        <label for="address" class="px-sm-1 pl-1 pt-sm-0 pt-2">My billing address is the same as the
                            shipping</label>
                        </div>
                        <div class="form-inline py-sm-2">
                        <input type="checkbox" name="details" id="details">
                        <label for="details" class="px-sm-2 pl-2 pt-sm-0 pt-2">Save my details for future purchases</label>
                    </div>
                    <div class="my-3">
                        <input type="submit" value="place your order" class="text-uppercase btn btn-primary btn-block p-3">
                    </div>
                    <div id="form-footer">
                        <p>By placing your order, you agree to our</p>
                        <p><a href="#">privacy notice</a> & <a href="#">terms of use</a>.</p>
                    </div>
                </form>
            </div>
        </div>
        <div class="BoiteDroite">
            <div class="offset-xl-2 col-xl-8 col-lg-12 col-md-12 col-sm-12 col-12 padding">
                <div class="card">
                <div class="card-header p-4">
                <a class="pt-2 d-inline-block" href="index.html" data-abc="true">BBBootstrap.com</a>
                <div class="float-right"> <h3 class="mb-0">Invoice #BBB10234</h3>
                Date: 12 Jun,2019</div>
                </div>
                <div class="card-body">
                <div class="row mb-4">
                <div class="col-sm-6">
                <h5 class="mb-3">From:</h5>
                <h3 class="text-dark mb-1">Tejinder Singh</h3>
                <div>29, Singla Street</div>
                <div>Sikeston,New Delhi 110034</div>
                <div>Email: contact@bbbootstrap.com</div>
                <div>Phone: +91 9897 989 989</div>
                </div>
                <div class="col-sm-6 ">
                <h5 class="mb-3">To:</h5>
                <h3 class="text-dark mb-1">Akshay Singh</h3>
                <div>478, Nai Sadak</div>
                <div>Chandni chowk, New delhi, 110006</div>
                <div>Email: info@tikon.com</div>
                <div>Phone: +91 9895 398 009</div>
                </div>
                </div>
                <div class="table-responsive-sm">
                <table class="table table-striped">
                <thead>
                <tr>
                <th class="center">#</th>
                <th>Item</th>
                <th>Description</th>
                <th class="right">Price</th>
                <th class="center">Qty</th>
                <th class="right">Total</th>
                </tr>
                </thead>
                <tbody>
                <tr>
                <td class="center">1</td>
                <td class="left strong">Iphone 10X</td>
                <td class="left">Iphone 10X with headphone</td>
                <td class="right">$1500</td>
                <td class="center">10</td>
                 <td class="right">$15,000</td>
                </tr>
                <tr>
                <td class="center">2</td>
                <td class="left">Iphone 8X</td>
                <td class="left">Iphone 8X with extended warranty</td>
                <td class="right">$1200</td>
                <td class="center">10</td>
                <td class="right">$12,000</td>
                </tr>
                <tr>
                <td class="center">3</td>
                <td class="left">Samsung 4C</td>
                <td class="left">Samsung 4C with extended warranty</td>
                <td class="right">$800</td>
                <td class="center">10</td>
                <td class="right">$8000</td>
                </tr>
                <tr>
                <td class="center">4</td>
                <td class="left">Google Pixel</td>
                <td class="left">Google prime with Amazon prime membership</td>
                <td class="right">$500</td>
                <td class="center">10</td>
                <td class="right">$5000</td>
                </tr>
                </tbody>
                </table>
                </div>
                <div class="row">
                <div class="col-lg-4 col-sm-5">
                </div>
                <div class="col-lg-4 col-sm-5 ml-auto">
                <table class="table table-clear">
                <tbody>
                <tr>
                <td class="left">
                <strong class="text-dark">Subtotal</strong>
                </td>
                <td class="right">$28,809,00</td>
                </tr>
                <tr>
                <td class="left">
                <strong class="text-dark">Discount (20%)</strong>
                </td>
                <td class="right">$5,761,00</td>
                </tr>
                <tr>
                <td class="left">
                <strong class="text-dark">VAT (10%)</strong>
                </td>
                <td class="right">$2,304,00</td>
                </tr>
                <tr>
                <td class="left">
                <strong class="text-dark">Total</strong>
                 </td>
                <td class="right">
                <strong class="text-dark">$20,744,00</strong>
                </td>
                </tr>
                </tbody>
                </table>
                </div>
                </div>
                </div>
                <div class="card-footer bg-white">
                <p class="mb-0">BBBootstrap.com, Sounth Block, New delhi, 110034</p>
                </div>
                </div>
                </div>
        </div>
    </div>    

    <?php include "footer.php"; ?>
</body>
</html>