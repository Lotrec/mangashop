<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="./produit.css">
</head>
<body>
    <?php include "header.php"; ?>
    
    <section class="formula1">
        <form action="../CONTROL/create.php" method="post">
            <div class="mb-3">
                <label for="formGroupExampleInput" class="form-label">Nom Manga</label>
                <input type="text" name="nom" class="form-control" id="formGroupExampleInput" placeholder="">
            </div>
            <div class="mb-3">
                <label for="formGroupExampleInput2" class="form-label">Images</label>
                <input type="text" name="image" class="form-control" id="formGroupExampleInput2" placeholder="">
            </div>
            <div class="mb-3">
                <label for="formGroupExampleInput3" class="form-label">Serie</label>
                <input type="text" name="serie" class="form-control" id="formGroupExampleInput3" placeholder="">
        </div>
        <div class="mb-3">
            <label for="formGroupExampleInput4" class="form-label">Prix</label>
            <input type="text" name="prix" class="form-control" id="formGroupExampleInput4" placeholder="">
        </div>
        <div class="mb-3">
            <label for="formGroupExampleInput5" class="form-label">Numero</label>
            <input type="text" name="numero" class="form-control" id="formGroupExampleInput5" placeholder="">
        </div>
        <div class="mb-3">
            <label for="formGroupExampleInput6" class="form-label">Quantité</label>
            <input type="text" name="quantite" class="form-control" id="formGroupExampleInput6" placeholder="">
        </div>
        <button type="submit" class="btn btn-primary">Ajouter</button>
    </form>
</section>

<?php
        
        require('../MODEL/data.php');
        $req = $pdo->query('select * from Produit');
        $mesInfos = $req->fetchAll();
        
        ?>

<div class="Container">
    <?php foreach($mesInfos as $data){?>
        
        <div class="Produit2">
            
            <div class="ImgProd" style="background: url('<?= $data['Image'] ?>') center; background-size: cover;" alt=""></div>
            <div class="NomProd"><?= $data['Nom'] ?></div>
            <div class="PrixProd"><?= $data['Prix'] ?></div>
            
            <form action="../CONTROL/update.php" method="post">
                
                <label for="inImage1">Image</label>
                <input type="text" name="image" id='inImage1' value="<?= $data["Image"] ?>"><br/>
                
                <label for="inNom1">Nom</label>
                <input type="text" name="nom" id='inNom1' value="<?= $data["Nom"] ?>"><br/>
                
                <label for="inPrix1">Prix</label>
                <input type="text" name="prix" id='inPrix1' value="<?= $data['Prix'] ?>"><br/>
                
                <label for="inQt1">serie</label>
                <input type="text" name="serie" id='inQt1' value="<?= $data['Serie'] ?>"><br/>
                
                <label for="inQt1">quantité</label>
                <input type="text" name="quantite" id='inQt1' value="<?= $data['Quantite'] ?>"><br/>
                
                <label for="inQt1">Numero</label>
                <input type="text" name="numero" id='inQt1' value="<?= $data['Numero'] ?>"><br/>
                
                <input type="hidden" name="id" value="<?= $data['ID'] ?>">
                <input type="submit" value="Modifier">
            </form>
                    
        </div>
        <?php } ?>
    </div>
</div>
    

<?php include "footer.php"; ?>

</body>
</html>