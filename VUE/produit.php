<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="produit.css">
    <title>Document</title>
</head>
<body>
<?php include "header.php"; ?>

<?php
        
        require('../MODEL/data.php');
        $req = $pdo->query('select * from Produit');
        $mesInfos = $req->fetchAll();
        
    ?>

    <div class="Container">
    <?php foreach($mesInfos as $data){?>

        <div class="Produit">
            <img src="<?php print_r($data["image"]);?>" alt="ImageProd" class="ImageProd">
            <div class="NomProd"><?= $data['nom'] ?></div>
            <div class="PrixProd"><?= $data['prix'] ?></div>
            <button class="AjoutPanier"><a href="detail.php?id=<?= $data['id'] ?>">Détail</a></button>
        </div>
    <?php } ?>
</body>
</html>