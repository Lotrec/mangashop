drop database if exists Mangashop;
CREATE DATABASE Mangashop;
USE Mangashop;

CREATE TABLE Client (
    ID INT AUTO_INCREMENT PRIMARY KEY,
    Nom VARCHAR (50),
    Mail VARCHAR (100)

);

CREATE TABLE Panier (
    ID INT AUTO_INCREMENT PRIMARY KEY,
    ID_Client INT,
    Datecrea DATE,
    Etat VARCHAR (50),
    Total DECIMAL (10, 2),
    FOREIGN KEY (ID_Client) REFERENCES Client(ID)
);

CREATE TABLE Produit (
    ID INT AUTO_INCREMENT PRIMARY KEY,
    Image VARCHAR (500),
    Nom VARCHAR(500),
    Numero INT,
    Serie VARCHAR(500),
    Prix INT,
    Quantite INT
);

insert into Produit(Image, Nom, Numero, Serie, prix, Quantite)
values (
    "https://furansujapon.com/wp-content/uploads/2022/01/naruto-tome-1-japonais.jpg",
    'Naruto',
    3,
    "Naruto series",
    22,
    10
);

insert into Produit(Image, Nom, Numero, Serie, prix, Quantite)
values (
    "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSHKwtO0H6xz7qppQWTOAZRcH90lta1OgAbsg&usqp=CAU",
    'berserk',
    2,
    "Berserk series",
    22,
    10
);

insert into Produit(Image, Nom, Numero, Serie, prix, Quantite)
values (
    "https://www.manga-news.com/public/images/series/naruto-anime-comics-cameleon.jpg",
    'Naruto shippuden',
    3,
    "Naruto series",
    22,
    10
);

CREATE TABLE Produit_Panier (
    ID_Prod INT,
    ID_Panier INT,
    Quantité INT,
    FOREIGN KEY(ID_Prod) REFERENCES Produit(ID),
    FOREIGN KEY(ID_Panier) REFERENCES Panier(ID)
);

DROP USER IF EXISTS abdel@localhost;
CREATE USER abdel@localhost IDENTIFIED BY 'mdp1234';
GRANT ALL PRIVILEGES ON Mangashop.* TO abdel@localhost;