<?php  ini_set('display_errors', 1); ini_set('display_startup_errors', 1); error_reporting(E_ALL); ?>
<?php
    include_once "debug.php";
    


    $host = 'localhost';
    $db   = 'Mangashop';
    $user = 'abdel';
    $pass = 'mdp1234'; 
    $pdo = new PDO("mysql:host=$host;dbname=$db;", $user, $pass);

    function NewProd ($Image, $Nom, $Numero, $Serie, $Prix, $Quantite){
        global $pdo;
        $req = $pdo->prepare("insert into Produit(Image, Nom, Numero, Serie, Prix, Quantite) values(?, ?, ?, ?, ?, ?);");
        $req->execute([$Image, $Nom, $Numero, $Serie, $Prix, $Quantite]);
    };

    function ReadAllProd (){
        global $pdo;
        $req = $pdo->query("select * from Produit");
        return $req->fetchAll();
    };

    function ReadProdByID ($ID){
        global $pdo;
        $req = $pdo->prepare("select * from Produit where ID =?");
        $req ->execute([$ID]);
        return $req -> fetchAll();
    };

    function UpdateProd($Id, $Image, $Nom, $Numero, $Serie, $Prix, $Quantite){
        global $pdo;
        $req = $pdo->prepare("UPDATE Produit SET Image=?, Nom=?, Numero=?, Serie=?, Prix=?, Quantite=? WHERE ID=?;");
        $req->execute([$Image, $Nom, $Numero, $Serie, $Prix, $Quantite, $Id]);
    };

    function DeleteProd ($ID){
        global $pdo;
        $req = $pdo->prepare("delete from Produit where ID=?;");
        $req->execute([$ID]);
    };